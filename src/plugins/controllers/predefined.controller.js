export default async function controller (f, options, done) {
  f.all('/:responseCode', async (request, reply) => {
    reply.code(request.params.responseCode)
    return { method: request.method, responseCode: request.params.responseCode }
  })

  done()
}
