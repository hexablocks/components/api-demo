export default async function controller (f, options, done) {
  f.all('*', async (request, reply) => {
    return {
      method: request.method,
      path: request.url,
      query: request.query,
      headers: request.headers,
      body: request.body
    }
  })

  done()
}
