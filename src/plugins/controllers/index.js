export default async function controllers (f, options, done) {
  f.register(import('./predefined.controller.js'), { prefix: '/predefined' })
  f.register(import('./echo.controller.js'), { prefix: '/echo' })

  f.log.info('Controllers ready')
  done()
}
