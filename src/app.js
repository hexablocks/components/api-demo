import fastify from 'fastify'

export default function app () {
  const logger = { level: process.env.LOG_LEVEL }
  if (process.env.LOG_PRETTY) {
    logger.transport = {
      target: 'pino-pretty'
    }
  }

  const f = fastify({ logger })

  // Los plugins tienen un timeout de registro de 10 segundos y no sé donde cambiarlo
  f.register(import('./plugins/controllers/index.js'))

  return f
}
