import dotenv from 'dotenv'
import app from './app.js'

dotenv.config()
const a = app()

async function start () {
  try {
    await a.listen({ port: process.env.SERVER_PORT, host: '0.0.0.0' })
  } catch (err) {
    a.log.error(err)
    process.exit(1)
  }
}
start()
